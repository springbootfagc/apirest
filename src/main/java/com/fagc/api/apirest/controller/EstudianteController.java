package com.fagc.api.apirest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fagc.api.apirest.model.Estudiante;

import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@RestController
@CrossOrigin(origins = "http://localhost:4200")//permite la conexion del fronEnd
public class EstudianteController {
	
	@Autowired
	private EstudianteService estudianteServices;
	
	@GetMapping("api/estudiante")
	public List<Estudiante> obtenerEstudiantes(){
		List<Estudiante> estudiantes = new ArrayList<>();
		
		Estudiante e1 = new Estudiante();
		e1.setId(4);
		e1.setNombres("Fabian");
		e1.setApellidos("Guerrero");
		e1.setEmail("fabian@mail.com");
		e1.setNota(4.5);
		
		Estudiante e2 = new Estudiante();
		e2.setId(5);
		e2.setNombres("Andrey");
		e2.setApellidos("Calvo");
		e2.setEmail("andrey@mail.com");
		e2.setNota(4.2);
		
		estudiantes.add(e1);
		estudiantes.add(e2);
		
		return estudiantes; 
	}
	
	@PostMapping("api/estudiantes")
	public Estudiante guardarEstudiante(@RequestBody Estudiante estudiante) {
		System.out.println(estudiante);
		estudianteServices.guardar(estudiante);
		return estudiante;
	}
	
	@GetMapping("api/estudiantes")
	public List<Estudiante> obtener(){
		return estudianteServices.obtenerTodos();
	}
        
        @GetMapping("api/estudiantes/{id}")
	public void obtenerEstudiante(@PathVariable("id")Integer id) {
		estudianteServices.obtenerEstudiante(id);
	}
        
        @PutMapping("api/estudiantes")
	public void actualizarEstudiante(@RequestBody Estudiante estudiante) {
		estudianteServices.actualizar(estudiante);
	}
        
        @DeleteMapping("api/estudiantes/{id}")
	public void elimnarEstudiante(@PathVariable("id")Integer id) {
		estudianteServices.eliminar(id);
	}	
	
}
